from django.conf.urls import patterns, include, url
from django.views.generic import ListView
from blog.models import Sensor_info
from blog.models import Sensor
from blog.models import Test
from blog import views

urlpatterns = patterns('',
					   # url(r'^', ListView.as_view(
							 # queryset = Post.objects.all().order_by("-date")[:10],
							 # template_name="blog.html")),

					   # 	url(r'^', ListView.as_view(
							 # queryset = Description.objects.all().order_by("-date")[:10],
							 # template_name="desc.html")),
						url(r'^$', views.index, name='index'),
			    		url(r'^detail/', views.detail, name='detail'),
			    		url(r'^download/$', views.download, name='download'),
			    		url(r'^Image/$', views.showStaticImage),
			    		url(r'^IMU_test/$', views.IMU),
					    url(r'^links/$', views.links, name='links'),
						url(r'^acc/$', views.acc, name='acc-mag'),
						url(r'^gyro/$', views.gyro, name='gyro'),
    				    url(r'^IMU_test/Alarm.png$', views.graph),
    				   # url(r'^graph_IMU2/$', views.graph2, name ="graph2"),
					   )