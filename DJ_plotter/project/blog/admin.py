from django.contrib import admin

# Register your models here.
from blog.models import Sensor_info
from blog.models import Sensor
from blog.models import Test
admin.site.register(Sensor)
admin.site.register(Sensor_info)
admin.site.register(Test)
