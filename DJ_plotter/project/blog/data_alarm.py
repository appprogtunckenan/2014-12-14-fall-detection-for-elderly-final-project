from visual import *
import time
import numpy as np
import matplotlib.pyplot as plt


import zmq

def db_data_analysis (alarm,index,ang_x_p,ang_y_p,ang_z_p,sensor_connected,ang_x_prev = 0,ang_y_prev = 0, ang_z_prev = 0,avg_vel =0):

	if (sensor_connected == True):
		string = socket.recv()
		xx = float(string[0:8])
		yy = float(string[9:17])
		zz = float(string[18:26])
	else:
		xx =1.0
		yy =1.0
		zz =1.0

	ang_x_val = -float(xx)
	ang_y_val = float(yy)
	ang_z_val = float(zz)

	ang_z_p.append(ang_z_val)
	ang_y_p.append(ang_y_val)

	ang_y_p.append(ang_y_val)
	if ang_x_val > 0:
		ang_x_val = ang_x_val
		ang_x_p.append(ang_x_val)
	else:
		ang_x_p.append(ang_x_val)


	if len(ang_x_p) > 100:
		ang_x_p.pop(0)
		ang_y_p.pop(0)
		ang_z_p.pop(0)


	ang_x_p_array = 0
	ang_y_p_array = 0
	ang_z_p_array = 0

	if index_record > 120:

		ang_x_p_array = np.array(ang_x_p, dtype=np.float32).transpose()
		ang_y_p_array = np.array(ang_y_p, dtype=np.float32).transpose()
		ang_z_p_array= np.array(ang_z_p, dtype=np.float32).transpose()

		d_ang_x_p = np.diff(ang_x_p_array)
		d_ang_y_p = np.diff(ang_y_p_array)
		d_ang_z_p = np.diff(ang_z_p_array)

		avg_vel = (max(d_ang_x_p) + max(d_ang_y_p) +max(d_ang_z_p))/3





		if avg_vel > vel_threshold:
			alarm = True
		else:
			alarm = False



	return alarm


ang_x_p = []
ang_y_p = []
ang_z_p = []
alarm_on = False
alarm = False
sensor_connected = False


context = zmq.Context()
socket = context.socket(zmq.SUB)
buff_size = 100
socket.setsockopt(zmq.SUBSCRIBE, "")

print "Collecting updates from the server..."
socket.connect("tcp://146.6.84.191:5556")

init = True

while True:
	index_record = 1
	vel_threshold = 20
	index_record = index_record +1
	alarm_on = db_data_analysis(alarm,index_record,ang_x_p,ang_y_p,ang_z_p,sensor_connected)
