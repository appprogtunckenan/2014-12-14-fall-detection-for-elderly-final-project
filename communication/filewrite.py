from visual import *
import time
import numpy as np
import matplotlib.pyplot as plt

import zmq

context = zmq.Context()
socket = context.socket(zmq.SUB)

socket.setsockopt(zmq.SUBSCRIBE, "")

print "Collecting updates from the server..."
socket.connect("tcp://146.6.84.191:5556")


global ang_x_prev,ang_y_prev,ang_z_prev,IMU,arrow_e1,arrow_e2,arrow_e3,ang_x_val,ang_y_val,ang_z_val,init

ang_x_prev =0
ang_y_prev =0
ang_z_prev =0

sensor_connected = False

# Animation starts here
def db_animation():

	global ang_x_prev,ang_y_prev,ang_z_prev,IMU,arrow_e1,arrow_e2,arrow_e3,ang_x_val,ang_y_val,ang_z_val,init

	if init == True:
		#Create objects for  animation
		IMU = box (pos=(0,0,0), length=4, height=0.5, width=4, color=color.white)


		ang_x = ang_x_val - ang_x_prev 
		ang_y = ang_y_val - ang_y_prev 
		ang_z = ang_z_val - ang_z_prev 

		xhat = vector( 10, 0, 0 )
		yhat = vector( 0, 10, 0 )
		zhat = vector( 0, 0, 10 )
		e1 = vector( xhat )
		e2 = vector( yhat )
		e3 = vector( zhat )

		arrow_e1 = arrow( pos=(0,0,0), axis=e1, color=color.red, shaftwidth=0.1 )
		arrow_e2 = arrow( pos=(0,0,0), axis=e2, color=color.green, shaftwidth=0.1 )
		arrow_e3 = arrow( pos=(0,0,0), axis=e3, color=color.blue, shaftwidth=0.1 )


		# initialize body frame unit vectors to initially point along space frame unit vectors

		e1 = vector( xhat )
		e2 = vector( yhat )
		e3 = vector( zhat )
		dt = 0.01

		IMU.rotate(angle=ang_x, axis=(1,0,0), origin=(0,0,0))
		IMU.rotate(angle=ang_y, axis=(0,1,0), origin=(0,0,0))
		IMU.rotate(angle=ang_z, axis=(0,0,1), origin=(0,0,0))

		arrow_e1.rotate(angle=ang_x, axis=(1,0,0), origin=(0,0,0))
		arrow_e1.rotate(angle=ang_y, axis=(0,1,0), origin=(0,0,0))
		arrow_e1.rotate(angle=ang_z, axis=(0,0,1), origin=(0,0,0))

		arrow_e2.rotate(angle=ang_x, axis=(1,0,0), origin=(0,0,0))
		arrow_e2.rotate(angle=ang_y, axis=(0,1,0), origin=(0,0,0))
		arrow_e2.rotate(angle=ang_z, axis=(0,0,1), origin=(0,0,0))

		arrow_e3.rotate(angle=ang_x, axis=(1,0,0), origin=(0,0,0))
		arrow_e3.rotate(angle=ang_y, axis=(0,1,0), origin=(0,0,0))
		arrow_e3.rotate(angle=ang_z, axis=(0,0,1), origin=(0,0,0))

	ang_x = ang_x_val - ang_x_prev 
	ang_y = ang_y_val - ang_y_prev 
	ang_z = ang_z_val - ang_z_prev 

	IMU.rotate(angle=ang_x, axis=(1,0,0), origin=(0,0,0))
	IMU.rotate(angle=ang_y, axis=(0,1,0), origin=(0,0,0))
	IMU.rotate(angle=ang_z, axis=(0,0,1), origin=(0,0,0))

	arrow_e1.rotate(angle=ang_x, axis=(1,0,0), origin=(0,0,0))
	arrow_e1.rotate(angle=ang_y, axis=(0,1,0), origin=(0,0,0))
	arrow_e1.rotate(angle=ang_z, axis=(0,0,1), origin=(0,0,0))

	arrow_e2.rotate(angle=ang_x, axis=(1,0,0), origin=(0,0,0))
	arrow_e2.rotate(angle=ang_y, axis=(0,1,0), origin=(0,0,0))
	arrow_e2.rotate(angle=ang_z, axis=(0,0,1), origin=(0,0,0))

	arrow_e3.rotate(angle=ang_x, axis=(1,0,0), origin=(0,0,0))
	arrow_e3.rotate(angle=ang_y, axis=(0,1,0), origin=(0,0,0))
	arrow_e3.rotate(angle=ang_z, axis=(0,0,1), origin=(0,0,0))

	ang_x_prev = ang_x_val
	ang_y_prev = ang_y_val
	ang_z_prev = ang_z_val

val = 1.0
xx =1.0
yy =1.0
zz =1.0
init = True
animation_on = True
while True:
	if (sensor_connected == True):
		string = socket.recv()
		xx = float(string[0:8])
		yy = float(string[9:17])
		zz = float(string[18:26])
	else:
		xx =xx +val
		yy =yy +val
		zz =zz +val



	ang_x_val = -float(xx)*pi/180
	ang_y_val = float(yy)*pi/180
	ang_z_val = float(zz)*pi/180

	index = 0
	if animation_on == True:

		rate(100)

		db_animation()
		init = False
		index = index + 1