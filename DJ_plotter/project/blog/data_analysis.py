from visual import *
import time
import numpy as np
import matplotlib.pyplot as plt


list_ang=[]
# f = open("file.txt","r+")
f = open("Free_Fall_Test.txt","r+")

for line in f:
	data = line.split()
	list_ang.append(data)

f.close()

global ang_x_prev,ang_y_prev,ang_z_prev,IMU,arrow_e1,arrow_e2,arrow_e3,ang_x_val,ang_y_val,ang_z_val,init


def db_data_analysis ():
	ang_x_p =[]
	ang_y_p =[]
	ang_z_p =[]
	time_sensor = []

	max = size(list_ang)/9 - 1

	time_current =0

	dt = 0.001;
	for index_plot in range(0,1000):


		ang_z_p.append(float(list_ang[index_plot][1]))

		ang_x_p_cur = float(list_ang[index_plot][2])
		ang_x_p.append(ang_x_p_cur)

		ang_y_p_cur = float(list_ang[index_plot][0])

		# if ang_x_p_cur > 0:
		# 	ang_x_p_cur = ang_x_p_cur -360
		# 	ang_x_p.append(ang_x_p_cur)
		# else:
			# ang_x_p.append(ang_x_p_cur)
		if ang_y_p_cur < 0:
			ang_y_p_cur = ang_y_p_cur +360
			ang_y_p.append(ang_y_p_cur)
		else:
			ang_y_p.append(ang_y_p_cur)



		time_sensor.append(time_current)
		time_current = index_plot+dt


	ang_x_p = np.array(ang_x_p, dtype=np.float32).transpose()
	ang_y_p = np.array(ang_y_p, dtype=np.float32).transpose()
	ang_z_p = np.array(ang_z_p, dtype=np.float32).transpose()
	time_sensor = np.array(time_sensor, dtype=np.float32).transpose()

	d_ang_x_p = np.diff(ang_x_p)
	d_ang_y_p = np.diff(ang_y_p)
	d_ang_z_p = np.diff(ang_z_p)


	plt.subplot(3, 1, 1)
	plt.plot(d_ang_x_p, 'g')
	plt.title('Orientation')
	plt.ylabel('pitch_vel')

	plt.subplot(3, 1, 2)
	plt.plot(d_ang_y_p, 'r')
	plt.ylabel('yaw_vel')

	plt.subplot(3, 1, 3)
	plt.plot(d_ang_z_p, 'b')
	plt.ylabel('roll_vel')
	plt.xlabel('time (s)')

	plt.show()


	plt.subplot(3, 1, 1)
	plt.plot(ang_x_p, 'g')
	plt.title('Orientation')
	plt.ylabel('pitch')

	plt.subplot(3, 1, 2)
	plt.plot(ang_y_p, 'r')
	plt.ylabel('yaw')

	plt.subplot(3, 1, 3)
	plt.plot(ang_z_p, 'b')
	plt.ylabel('roll')
	plt.xlabel('time (s)')

	plt.show()

db_data_analysis()



IMU = box (pos=(0,0,0), length=4, height=0.5, width=4, color=color.white)


ang_x_val = -float(list_ang[0][2])*pi/180
ang_y_val = float(list_ang[0][0])*pi/180
ang_z_val = float(list_ang[0][1])*pi/180

ang_x_prev=0
ang_y_prev=0
ang_z_prev=0

ang_x = ang_x_val - ang_x_prev 
ang_y = ang_y_val - ang_y_prev 
ang_z = ang_z_val - ang_z_prev 

xhat = vector( 10, 0, 0 )
yhat = vector( 0, 10, 0 )
zhat = vector( 0, 0, 10 )
e1 = vector( xhat )
e2 = vector( yhat )
e3 = vector( zhat )

arrow_e1 = arrow( pos=(0,0,0), axis=e1, color=color.red, shaftwidth=0.1 )
arrow_e2 = arrow( pos=(0,0,0), axis=e2, color=color.green, shaftwidth=0.1 )
arrow_e3 = arrow( pos=(0,0,0), axis=e3, color=color.blue, shaftwidth=0.1 )


# initialize body frame unit vectors to initially point along space frame unit vectors

e1 = vector( xhat )
e2 = vector( yhat )
e3 = vector( zhat )
dt = 0.01


IMU.rotate(angle=ang_x, axis=(1,0,0), origin=(0,0,0))
IMU.rotate(angle=ang_y, axis=(0,1,0), origin=(0,0,0))
IMU.rotate(angle=ang_z, axis=(0,0,1), origin=(0,0,0))

arrow_e1.rotate(angle=ang_x, axis=(1,0,0), origin=(0,0,0))
arrow_e1.rotate(angle=ang_y, axis=(0,1,0), origin=(0,0,0))
arrow_e1.rotate(angle=ang_z, axis=(0,0,1), origin=(0,0,0))

arrow_e2.rotate(angle=ang_x, axis=(1,0,0), origin=(0,0,0))
arrow_e2.rotate(angle=ang_y, axis=(0,1,0), origin=(0,0,0))
arrow_e2.rotate(angle=ang_z, axis=(0,0,1), origin=(0,0,0))

arrow_e3.rotate(angle=ang_x, axis=(1,0,0), origin=(0,0,0))
arrow_e3.rotate(angle=ang_y, axis=(0,1,0), origin=(0,0,0))
arrow_e3.rotate(angle=ang_z, axis=(0,0,1), origin=(0,0,0))

index = 0
while 1:
	rate (60)
	ang_x_val = -float(list_ang[index][2])*pi/180
	ang_y_val = float(list_ang[index][0])*pi/180
	ang_z_val = float(list_ang[index][1])*pi/180

	ang_x = ang_x_val - ang_x_prev 
	ang_y = ang_y_val - ang_y_prev 
	ang_z = ang_z_val - ang_z_prev 

	IMU.rotate(angle=ang_x, axis=(1,0,0), origin=(0,0,0))
	IMU.rotate(angle=ang_y, axis=(0,1,0), origin=(0,0,0))
	IMU.rotate(angle=ang_z, axis=(0,0,1), origin=(0,0,0))

	arrow_e1.rotate(angle=ang_x, axis=(1,0,0), origin=(0,0,0))
	arrow_e1.rotate(angle=ang_y, axis=(0,1,0), origin=(0,0,0))
	arrow_e1.rotate(angle=ang_z, axis=(0,0,1), origin=(0,0,0))

	arrow_e2.rotate(angle=ang_x, axis=(1,0,0), origin=(0,0,0))
	arrow_e2.rotate(angle=ang_y, axis=(0,1,0), origin=(0,0,0))
	arrow_e2.rotate(angle=ang_z, axis=(0,0,1), origin=(0,0,0))

	arrow_e3.rotate(angle=ang_x, axis=(1,0,0), origin=(0,0,0))
	arrow_e3.rotate(angle=ang_y, axis=(0,1,0), origin=(0,0,0))
	arrow_e3.rotate(angle=ang_z, axis=(0,0,1), origin=(0,0,0))

	ang_x_prev = ang_x_val
	ang_y_prev = ang_y_val
	ang_z_prev = ang_z_val

	index = index +1