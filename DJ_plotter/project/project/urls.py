from django.conf.urls import patterns, include, url
from django.contrib import admin
from blog import views
from django.views.generic import ListView

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'mysite.views.home', name='home'),
    				# url(r'^blog/', include('blog.urls')),

					url(r'^admin/', include(admin.site.urls)),
    				url(r'^blog/', include('blog.urls')),)

					# url(r'^graph_IMU2/$', views.graph2, name ="graph2"),)