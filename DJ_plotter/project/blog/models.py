import datetime

from django.db import models
from django.utils import timezone


class Sensor(models.Model):
	sensor_name = models.CharField(max_length=200)
	pub_date = models.DateTimeField('sensor added date')
	def __str__(self):
		return self.sensor_name
# Create your models here.
class Sensor_info(models.Model):
	info = models.ForeignKey(Sensor)
	sensor_description = models.CharField(max_length=200)
	def __str__(self):
		return self.sensor_description

class Test(models.Model):
	test_text = models.CharField(max_length=200)
	test_date = models.DateTimeField('date tested')
	def __str__(self):
		return self.test_text