#from django.shortcuts import render

# Create your views here.

# from django.shortcuts import render

from matplotlib import pylab
from pylab import *
import PIL
import PIL.Image
from PIL import Image
import StringIO
import django
from django.template import RequestContext, loader
from django.shortcuts import get_object_or_404,render
from django.http import HttpResponse,HttpResponseRedirect,Http404
from blog.models import Sensor,Sensor_info,Test

from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
from matplotlib.dates import DateFormatter
import time
import random

from visual import *
import time
import numpy as np
import matplotlib.pyplot as plt


import zmq


def index(request):
	latest_sensor_list = Sensor.objects.order_by('-pub_date')[:5]
	template = loader.get_template('index.html')
	context = RequestContext(request, {
        'latest_sensor_list': latest_sensor_list,
    })
	return HttpResponse(template.render(context))

def download(request):
	return HttpResponseRedirect('https://docs.google.com/document/d/14mmM1Y9KUXVNNK_cOKtx07yBwkubqr5SYH8daFwXDNA/pub')


def detail(request):
	sensor_id = 2
	sensor_info = get_object_or_404(Sensor, pk=sensor_id)
	return render(request, 'blog/detail.html')

def showStaticImage(request):
	
	imagePath = "C:/Users/tunc/Desktop/DJ_plotter/project/blog/sensor.png"

	Image.init()
	i = Image.open(imagePath)
	response = HttpResponse(content_type ='image/png')
	i.save(response,'PNG')
	return response

def links(request):

	return HttpResponseRedirect('http://www.pololu.com/product/1268')

def acc(request):

	return HttpResponseRedirect('http://www.pololu.com/file/download/LSM303DLHC.pdf?file_id=0J564')

def gyro(request):

	return HttpResponseRedirect('http://www.pololu.com/file/download/L3GD20.pdf?file_id=0J563')


def IMU(request):
    return render(request, 'IMU.html')




def db_data_analysis (alarm,index,ang_x_p,ang_y_p,ang_z_p,sensor_connected,ang_x_prev = 0,ang_y_prev = 0, ang_z_prev = 0,avg_vel =0):


	if (sensor_connected == True):
		string = socket.recv()
		xx = float(string[0:8])
		yy = float(string[9:17])
		zz = float(string[18:26])
	else:
		xx =1.0
		yy =1.0
		zz =1.0
	
	ang_x_val = -float(xx)
	ang_y_val = float(yy)
	ang_z_val = float(zz)

	ang_z_p.append(ang_z_val)
	ang_y_p.append(ang_y_val)

	ang_y_p.append(ang_y_val)
	if ang_x_val > 0:
		ang_x_val = ang_x_val
		ang_x_p.append(ang_x_val)
	else:
		ang_x_p.append(ang_x_val)


	if len(ang_x_p) > 100:
		ang_x_p.pop(0)
		ang_y_p.pop(0)
		ang_z_p.pop(0)


	ang_x_p_array = 0
	ang_y_p_array = 0
	ang_z_p_array = 0

	if index > 120:

		ang_x_p_array = np.array(ang_x_p, dtype=np.float32).transpose()
		ang_y_p_array = np.array(ang_y_p, dtype=np.float32).transpose()
		ang_z_p_array= np.array(ang_z_p, dtype=np.float32).transpose()

		d_ang_x_p = np.diff(ang_x_p_array)
		d_ang_y_p = np.diff(ang_y_p_array)
		d_ang_z_p = np.diff(ang_z_p_array)

		avg_vel = (max(d_ang_x_p) + max(d_ang_y_p) +max(d_ang_z_p))/3


		if avg_vel > vel_threshold:
			alarm = True
		else:
			alarm = False



	return alarm






def graph(request):

	ang_x_p = []
	ang_y_p = []
	ang_z_p = []

	context = zmq.Context()
	socket = context.socket(zmq.SUB)
	buff_size = 100
	socket.setsockopt(zmq.SUBSCRIBE, "")

	print "Collecting updates from the server..."
	socket.connect("tcp://146.6.84.191:5556")

	alarm_on = False
	alarm = False
	sensor_connected = False
	while True:

		if alarm_on == True:

			imagePath = "C:/Users/tunc/Desktop/alarm1.png"
		else:
			imagePath = "C:/Users/tunc/Desktop/normal.png"

		Image.init()
		i = Image.open(imagePath)
		response = HttpResponse(content_type ='image/png')
		i.save(response,'PNG')


		index_record = 1
		vel_threshold = 20
		index_record = index_record +1
		alarm_on = db_data_analysis(alarm,index_record,ang_x_p,ang_y_p,ang_z_p,sensor_connected)

		return response


